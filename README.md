# Fraud Strike CSGO

For frauds only.

# Setup

Download MIGI v3 from ZooL:

[Direct link (current as of 6 July 2021)](https://github.com/ZooLSmith/MIGI3/raw/main/migi.exe)

[Invite link to ZooL's server](http://zoolsmith.free.fr/discord)

Once you have MIGI installed, run the program as admin (it shouldn't start when you do this), and then run the program normally.

Once MIGI is operational, place the p_ folders from this repository in Counter-Strike Global Offensive\migi\csgo\addons\

Once your folders are in the correct place, you should see the addons listed in MIGI. Build and launch, and you should be good to go!

# Known Issues

- **Some equipment is available for the wrong sides, or not at all.**
- By default, only certain weapons are equipped (e.g. CZ-75 OR Tec-9). You can only fix this by independently equipping weapons for the appropriate sides, which is a critical issue that cannot be worked around.

- **Grenades (and healthshots) disappear when selected.**
- Valve introduced a new bug in Operation Riptide that rendered grenades and healthshots useless unless you purchase multiple of them while also having a primary or secondary equipped. I am not sure if this can be fixed. I will lower their price in the meantime to allow for cheaper workarounds, but it has completely broken bot grenade support as of right now.

- **Buy binds don't translate cleanly.**
- Buy binds that refer to classic weapon arrangements are often inconsistent with the new weapon arrangements in this game mode. I'll compile a list of old and new aliases eventually, so that players can translate their buy binds if they wish.

- **Bots sometimes buy two weapons for the same equipment slot.**
- Bot buys look to the old weapon slots. I am not sure if this can be fixed.

# FAQ

**Q: Why?**

**A:** Wanted something more fun to play in Fraud Nights!

**Q: Will I get VAC'd?**

**A:** Nah, in my experience you just get kicked out for invalid files.

**Q: I want to join the club! How do I do that?**

**A:** The No-Frauds Club is a community run primarily through Discord. You can join our server from [this invite link](https://discord.gg/s5SKBmY​).